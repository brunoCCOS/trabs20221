#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

//struct para simular as informações de um processo.
typedef struct {
    int index;
    int PID;
    int chegadaNaFila;
    int total;
    int totalRodado;
    int IO[3];
    int tempoParaIO[3];
}processos;

//struct para se criar as filas ordenadas.
typedef struct filaProcessos{
    processos *atual;
    struct filaProcessos *proximo;
}filaProcessos;

//tentativa de criação da lista de alta prioridade a partir dos valores de chegada do programa
void escalonador(processos *ptrLista, filaProcessos *ptrFilaA, int ut, int qnt, int *k){
    filaProcessos *aux, *aux2 = ptrFilaA;
    for(int i=0; i<qnt; i++){
        if(ptrLista[i].chegadaNaFila == ut){
            *k=*k+1;
            aux = malloc(sizeof(filaProcessos));
            aux->atual = &ptrLista[i];
            aux->proximo = 0;
            while(aux2->proximo){
                aux2 = aux2->proximo;
            }
            if(*k==1){
                aux2->atual= aux->atual;
                }//se primeiro da lista coloca no lugar certo
            else{
                aux2->proximo = aux;
                }//os outros no lugar certo
            //getchar();//pausar qndo chega aqui para checagem
            //printf("%d, %d",aux2->atual.index, aux2->proximo->atual.index);
        }
    }
}

void gerenciadorFilas(filaProcessos *ptrFilaA, filaProcessos *ptrFilaB, filaProcessos *terminados, int ut, int tamanhoCiclo, int *quantidadeTerminados){
    filaProcessos *aux=ptrFilaB, *aux2=terminados;
    if(ptrFilaA->atual){
        printf("passei do primeiro if");
        getchar();
        if(ptrFilaA->atual){//verifica se há alguem no inicio da fila
            ptrFilaA->atual->totalRodado+=tamanhoCiclo;//"roda o processo"
            if(ptrFilaA->atual->totalRodado >= ptrFilaA->atual->total){//verifica se o programa ja rodou tudo o que tinha q ser rodado.
                *quantidadeTerminados+=1;
                while(aux2->proximo){//atualiza a fila de terminados ate seu ultimo membro
                    aux2=aux2->proximo;
                }
                if(*quantidadeTerminados==1){
                    aux2->atual= ptrFilaA->atual;
                }//se primeiro a terminar, coloca no primeiro lugar da lista
                else{
                    filaProcessos *aux3=malloc(sizeof(filaProcessos));
                    aux3->atual=ptrFilaA->atual;
                    aux2->proximo=aux3;
                    ptrFilaA=ptrFilaA->proximo;
                }//coloca o item no final da fila
            }
            else{
                while(aux->proximo){
                    aux=aux->proximo;
                }//atualiza a fila de baixa prioridade ate o ultimo membro
                if(aux->atual == 0 && aux->proximo == 0){
                    aux->atual= ptrFilaA->atual;
                    ptrFilaA= ptrFilaA->proximo;
                }//se primeiro a terminar, coloca no primeiro lugar da lista
                else{
                    filaProcessos *aux3=malloc(sizeof(filaProcessos));
                    aux3->atual=ptrFilaA->atual;
                    aux->proximo=aux3;
                    ptrFilaA=ptrFilaA->proximo;
                }//coloca o item no final da fila
            }
        }
    }
}

void main( int argc, char *argv[]){
    
    srand(time(NULL));
    int qntPro = atoi(argv[1]);
    //iniciação dos parametros
    processos *ptrInit;
    filaProcessos *ptrFilaAlta, *ptrFilaBaixa,*ptrFilaIoD, *ptrFilaIoF, *ptrFilaIoI, *auxiliar, *ptrTerminados;
    
    int temposIO[3] = {3, 8, 12};
    ptrInit = malloc(qntPro*sizeof(processos));
    ptrFilaAlta = malloc(sizeof(filaProcessos));
    ptrFilaBaixa = malloc(sizeof(filaProcessos));
    ptrFilaIoD = malloc(sizeof(filaProcessos));
    ptrFilaIoF = malloc(sizeof(filaProcessos));
    ptrFilaIoI = malloc(sizeof(filaProcessos));
    ptrTerminados = malloc(sizeof(filaProcessos));
    
    if(!ptrInit || !ptrFilaAlta || !ptrFilaBaixa || !ptrFilaIoD || !ptrFilaIoF || !ptrFilaIoI || !ptrTerminados){
        printf("Erro de inicialização dos ponteiros");
        return;
        //check do malloc
    }
    ptrFilaAlta->proximo = 0;
    ptrFilaBaixa->proximo = 0;
    ptrFilaIoD->proximo = 0;
    ptrFilaIoF->proximo = 0;
    ptrFilaIoI->proximo = 0;
    printf("%p\n\n", ptrFilaAlta);
    //criação dos processos com coisas aleatorias
    for(int i=0;i<qntPro;i++){
        ptrInit[i].index = i+1;
        ptrInit[i].PID = &ptrInit[i];
        ptrInit[i].chegadaNaFila = 0;
        while(!ptrInit[i].chegadaNaFila){
            ptrInit[i].chegadaNaFila = rand()%(2*qntPro+1);
            for(int j=0;j<i;j++){
                if(ptrInit[j].chegadaNaFila == ptrInit[i].chegadaNaFila){ 
                    ptrInit[i].chegadaNaFila = 0;
                    break;
                }
            }
        }
        ptrInit[i].total = 0;
        while(!ptrInit[i].total){
            ptrInit[i].total = rand()%17;
            if(ptrInit[i].total < 5) ptrInit[i].total=0;
        }
        ptrInit[i].totalRodado = 0;
        for(int j=0;j<3;j++){
            ptrInit[i].IO[j] = rand()%2;
            ptrInit[i].tempoParaIO[j] = ptrInit[i].IO[j]* temposIO[j];
        }
    }
    //print dos processos criados
    /*printf("Lista de Processos:\n");
    for (int i = 0; i < atoi(argv[1]); i++)
    {
        printf("Processo %d:\n", i+1);
        printf("\tPID: %x Ox\n", ptrInit[i].PID);
        printf("\tChegada: %d\n", ptrInit[i].chegadaNaFila);
        printf("\tTotal a ser rodado: %d u.t\n", ptrInit[i].total);
        printf("\tTotal Rodado: %d u.t\n", ptrInit[i].totalRodado);
        printf("\tIO necessarios e seus tempos:\n");
        for(int j=0;j<3;j++){
            if(ptrInit[i].IO[j]&&j==0) printf("\t\tDisco e %d u.t\n", ptrInit[i].tempoParaIO[j]);
            if(ptrInit[i].IO[j]&&j==1) printf("\t\tFita e %d u.t\n", ptrInit[i].tempoParaIO[j]);
            if(ptrInit[i].IO[j]&&j==2) printf("\t\tImpressora e %d u.t", ptrInit[i].tempoParaIO[j]);
        }
    printf("\n");
    printf("\n");   
    }*/
    getchar();
    //teste da criação da lista.
    int contagem = 0, terminados=0;
    for(int i = 0; i<60; i++){
        escalonador(ptrInit, ptrFilaAlta, i, qntPro, &contagem);
        gerenciadorFilas(ptrFilaAlta,ptrFilaBaixa, ptrTerminados, i, 4, &terminados);
        if(i==20) contagem = 0;
    }
    int i = 0;
    do{
    printf("%p %d %p\n", ptrFilaAlta, ptrFilaAlta->atual->totalRodado, ptrFilaAlta->proximo);
    ptrFilaAlta=ptrFilaAlta->proximo;
    i++;
    }while(i<qntPro);
    }

